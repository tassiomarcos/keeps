import React from 'react';
import { StyleSheet } from 'react-native';
import { View, Text } from 'react-native';
import { verticalScale, width } from '../../utils/dimensions';
import {LinearGradient} from 'expo';
// const styles = StyleSheet.create({
//    backgroundColor: #333ddd,
//    height: verticalScale(80),
// })

const styles = StyleSheet.create({
    width: width,
    height: verticalScale(130),
 })

export const Header = () => (
        <LinearGradient colors={['#c88401', '#ae2d9c']} style={styles} >
            <Text>Tassio</Text>
        </LinearGradient>
)