import React from 'react';
import { View } from 'react-native';

const style = {
    flex: 1,
}


export const Container = ({ children, backgroundColor }) => (
    <View style={{...style, backgroundColor }}>
        { children }
    </View>
)
