import { Platform, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window');
const iPhoneX = Platform.OS === 'ios' && width === 812 || height === 812;


const base_unit = 512;
const verticalScale = size => height / base_unit * size;

export {
    iPhoneX,
    verticalScale,
    width
}